/*
 * Copyright (c) 2010 - 2011, Open Wonderland Foundation. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *  . Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  . Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  . Neither the name of Open Wonderland Foundation, nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Copyright (c) 2009, Sun Microsystems, Inc. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *  . Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  . Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  . Neither the name of Sun Microsystems, Inc., nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.jdesktop.mtgame;

import com.jme3.renderer.Camera;
import com.jme3.scene.Spatial;
import com.jme3.system.lwjgl.LwjglCanvas;
import java.awt.Canvas;
import javolution.util.FastList;

import org.jdesktop.mtgame.RenderComponent.EntityRef;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GLContext;

/**
 * This class encapsultes a rendering surface in mtgame.  It can be used
 * for may different purposes.  It can be used for onscreen rendering, texture
 * rendering, and shadow map rendering.
 * 
 * @author Doug Twilleager
 */
public class OnscreenRenderBuffer extends RenderBuffer {
    /**
     * The Canvas used for ONSCREEN rendering
     */
    private LwjglCanvas canvas = null;

    /**
     * The current GLContext
     */
    private GLContext glContext = null;

    /**
     * The current GL object
     */
    private GL20 gl = null;

    /**
     * A boolean that indicates that we should do a clear
     */
    private boolean doClear = true;

    /**
     * A boolean that indicates  that we should do a swap
     */
    private boolean doSwap = true;

    /**
     * The constructor
     */
    public OnscreenRenderBuffer(Target target, int width, int height, int order) {
        super(target, width, height, order);
    }

    /**
     * Set the canvas in onscreen mode
     */
    void setCanvas(LwjglCanvas c) {
        canvas = c;
    }

    /**
     * Get the onscreen canvas.
     */
    public LwjglCanvas getCanvas() {
        return (canvas);
    }

    /**
     * Set the swap flag
     */
    public void setSwapEnable(boolean flag) {
        doSwap = flag;
    }

    /**
     * Get the swap flag
     */
    public boolean getSwapEnable() {
        return (doSwap);
    }


    /**
     * Set the clear flag
     */
    public void setClearEnable(boolean flag) {
        doClear = flag;
    }

    /**
     * Get the clear flag
     */
    public boolean getClearEnable() {
        return (doClear);
    }

    /**
     * This gets called to clear the buffer
     */
    public void clear(com.jme3.renderer.Renderer renderer) {
        if (doClear) {
//            renderer.clearBuffers();
//            renderer.clearStencilBuffer();
        }
    }

    /**
     * This gets called to make this render buffer current for rendering
     */
    public boolean makeCurrent(Object display, com.jme3.renderer.Renderer jMERenderer) {
        boolean doRender = true;
//        GLCanvas currentCanvas = (GLCanvas) canvas;
//        glContext = currentCanvas.getContext();
//        try {
//            glContext.makeCurrent();
//        } catch (javax.media.opengl.GLException e) {
//            System.out.println(e);
//        }
//        gl = glContext.getGL();
//
//
//        CameraComponent cc = getCameraComponent();
//        if (cc != null) {
//            cc.update();
//            Camera camera = cc.getCamera();
//            if (getWidth() != canvas.getWidth() ||
//                getHeight() != canvas.getHeight()) {
//                setWidth(canvas.getWidth());
//                setHeight(canvas.getHeight());
//                camera.resize(canvas.getWidth(), canvas.getHeight());
//            }
//            camera.update();
//            jMERenderer.setCamera(camera);
//            camera.apply();
//            jMERenderer.setBackgroundColor(backgroundColor);
//        } else {
//            doRender = false;
//        }
        return (doRender);
    }

    /**
     * These are used to render the given opaque, transparent, and ortho objects
     */
    public void preparePass(com.jme3.renderer.Renderer renderer, FastList<Spatial> renderList, FastList<PassComponent> passList, int pass) {
//        renderer.clearQueue();

        Portal p = getPortal();
        if (p != null) {
//            renderer.getQueue().setPortalGeometry(p.getGeometry(), lastLoc, lastDir, lastUp, lastLeft);
        }

        if (getManageRenderScenes()) {
            synchronized (renderComponentList) {
                renderList(renderer, getManagedRenderList());
                renderPassList(renderer, managedPassList);
            }
        } else {
            renderList(renderer, renderList);
            renderPassList(renderer, passList);
        }
    }

    public void completePass(com.jme3.renderer.Renderer renderer, int pass) {
        // Nothing to do
//        renderer.renderQueue();
//        renderer.getQueue().setPortalGeometry(null, null, null, null, null);
    }

    public void renderOpaque(com.jme3.renderer.Renderer renderer) {
    }

    public void renderPass(com.jme3.renderer.Renderer renderer) {
    }

    public void renderTransparent(com.jme3.renderer.Renderer renderer) {
    }

    public void renderOrtho(com.jme3.renderer.Renderer renderer) {
    }

    private void renderPassList(com.jme3.renderer.Renderer renderer, FastList<PassComponent> list) {
        for (int i=0; i<list.size(); i++) {
            PassComponent pc = (PassComponent) list.get(i);
//            pc.getPass().renderPass(renderer);
        }
    }

    private void renderList(com.jme3.renderer.Renderer renderer, FastList<Spatial> list) {
        for (int i=0; i<list.size(); i++) {
            Spatial s = list.get(i);

            // record the render start time
            long startTime = System.nanoTime();
            
//            renderer.draw(s);
            
            // find the associated entity and update statistics if the entity
            // has a statistics component
            EntityRef er = (EntityRef) s.getUserData(RenderComponent.ENTITY_KEY);
            if (er != null && er.getEntity() != null) {
                StatisticsComponent sc =
                        er.getEntity().getComponent(StatisticsComponent.class);
                if (sc != null) {
                    sc.setLastRenderTime(System.nanoTime() - startTime);
                }
            }
        }
    }

    /**
     * This is called when a frame has completed
     */
    public void release() {
//        glContext.release();
    }

    /**
     * This is called when the buffer needs to be swaped
     */
    public void swap() {
        if (doSwap) {
//            canvas.swapBuffers();
        }
    }
}
